PWMMode = 2
ADCMode = 3
pins = {D0=gpio.OUTPUT,
        D1=gpio.OUTPUT,
        D2=gpio.OUTPUT,
        D3=gpio.OUTPUT,
        D4=gpio.OUTPUT,
        D5=PWMMode,
        D6=gpio.INPUT,
        D7=gpio.INPUT,
        D8=gpio.OUTPUT,
        A0=ADCMode}

for key, val in pairs(pins) do
    local pinNr = tonumber(string.sub(key, 2))
    if (val==PWMMode) then
        pwm.setup(pinNr, 100, 0)
        pwm.start(pinNr)
    elseif (val==gpio.OUTPUT or val==gpio.INPUT) then
        gpio.mode(pinNr, val)
        gpio.write(pinNr, gpio.LOW)
    end
end

srv=net.createServer(net.TCP) 
srv:listen(80,function(conn) 
    conn:on("receive", function(client,request)
        local buf = "var modeVals='";
        local _, _, method, path, vars = string.find(request, "([A-Z]+) (.+)?(.+) HTTP");
        if(method == nil)then 
            _, _, method, path = string.find(request, "([A-Z]+) (.+) HTTP"); 
        end
        local _GET = {}
        if (vars ~= nil)then 
            for k, v in string.gmatch(vars, "(%w+)=(%w+)&*") do 
                _GET[k] = v 
            end 
        end

        for key,val in pairs(_GET) do        
            local pinNr = tonumber(string.sub(key, 2))
            local pinType = string.sub(key, 1, 1)
            if (pins[key] == PWMMode) then
                pwm.setduty(pinNr, val)
            elseif (pins[key] == gpio.OUTPUT) then
                gpio.write(pinNr, val)
            end
        end

        for key, val in pairs(pins) do
            local pinNr = tonumber(string.sub(key, 2))
            buf = buf..key.."@"..val.."@"
            if (val == PWMMode) then
                buf = buf..pwm.getduty(pinNr).."|"
            elseif (val == ADCMode) then
                buf = buf..adc.read(0).."|"                        
            else
                buf = buf..gpio.read(pinNr).."|"                
            end
        end

        buf = buf.."';"

        client:send(buf);
        client:close();
        collectgarbage();
    end)
end)
